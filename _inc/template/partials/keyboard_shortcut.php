<div class="table-responsive">
<table class="table table-bordered table-striped table-condensed">
<thead>
	<tr class="active">
		<th class="text-center">Atajo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td class="text-center">
			<kbd>Alt + P</kbd>
		</td>
		<td>Busqueda de producto</td>
	</tr>
	<tr>
		<td class="text-center">
			<kbd>Alt + C</kbd>
		</td>
		<td>Busqueda de cliente</td>
	</tr>
	<tr>
		<td class="text-center">
			<kbd>Alt + A</kbd>
		</td>
		<td>Agregar cliente</td>
	</tr>
	<tr>
		<td class="text-center">
			<kbd>Alt + I</kbd>
		</td>
		<td>Campo de descuento</td>
	</tr>
	<tr>
		<td class="text-center">
			<kbd>Alt + T</kbd>
		</td>
		<td>Campo de impuesto</td>
	</tr>
	<tr>
		<td class="text-center">
			<kbd>Alt + S</kbd>
		</td>
		<td>Costo de envío</td>
	</tr>
	<tr>
		<td class="text-center">
			<kbd>Alt + O</kbd>
		</td>
		<td>Otros costos</td>
	</tr>
	<tr>
		<td class="text-center">
			<kbd>Alt + H</kbd>
		</td>
		<td>Orden de espera</td>
	</tr>
	<tr>
		<td class="text-center">
			<kbd>Alt + Z</kbd>
		</td>
		<td>Pagar ahora</td>
	</tr>
</tbody>
</table>
</div>