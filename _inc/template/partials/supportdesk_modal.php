<?php include ("../../../_init.php");?>
<div class="box-body">
	<div class="logo text-center mb-20">
		<img src="<?php echo root_url();?>/assets/itsolution24/logo.jpg" alt="logo" width="120px;height:120px;">
	</div>
	<div class="panel mb-20" style="border:2px dotted #ddd;">
		<div class="panel-body">
			<p class="text-center"><b>Para cualquier consulta simplemente llámenos a través del cuadro de comentarios</b></p>
			<h4 class="text-center">
			    <a href="#">Documentación en línea aquí</a>
			</h4>
		</div>
	</div>
	<div class="row mb-20" style="margin-right:0;">
		<div class="col-sm-4">
			<a class="btn btn-info btn-block" href="#">Envíanos un correo electrónico &rarr;</a>
		</div>
		<div class="col-sm-4">
			<a class="btn btn-warning btn-block" href="#" target="_blank">Síguenos &rarr;</a>
		</div>
		<div class="col-sm-4">
			<a class="btn btn-success btn-block" href="#" target="_blank">Contacta con nosotros &rarr;</a>
		</div>	
	</div>
	<br>
	<p class="mt-20 text-center"><i>Gracias por elegirnos!</i></p>
</div>
