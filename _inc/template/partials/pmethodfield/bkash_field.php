<div class="panel panel-default mt-5">
	<div class="panel-body">
		<div class="form-group">
	      <label for="bkash_trx_id" class="col-sm-4 control-label">
	        ID Trx
	      </label>
	      <div class="col-sm-8">
	        <input type="text" id="bkash_trx_id" name="payment_details[bkash_trx_id]" placeholder="ID Trx" class="form-control" autocomplete="off">
	      </div>
	    </div>
	    <div class="form-group">
	      <label for="bkash_phone_no" class="col-sm-4 control-label">
		  Teléfono móvil
	      </label>
	      <div class="col-sm-8">
	        <input type="text" id="bkash_phone_no" name="payment_details[bkash_phone_no]" placeholder="Teléfono móvil" class="form-control" autocomplete="off">
	      </div>
	    </div>
	</div>
</div>