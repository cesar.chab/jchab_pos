<div class="panel panel-default mt-5">
	<div class="panel-body">
		<div class="form-group">
	      <label for="card_number" class="col-sm-4 control-label">
		  Número de tarjeta
	      </label>
	      <div class="col-sm-8">
	        <input type="text" id="card_number" name="payment_details[card_number]" placeholder="Número de tarjeta" class="form-control" autocomplete="off">
	      </div>
	    </div>
	    <div class="form-group">
	      <label for="expiration_date" class="col-sm-4 control-label">
		  Fecha de caducidad
	      </label>
	      <div class="col-sm-8">
	        <input type="month" id="expiration_date" name="payment_details[expiration_date]" placeholder="Fecha de caducidad" class="form-control" autocomplete="off">
	      </div>
	    </div>
	    <div class="form-group">
	      <label for="card_security_code" class="col-sm-4 control-label">
		  Código de Seguridad de la Tarjeta
	      </label>
	      <div class="col-sm-8">
	        <input type="text" id="card_security_code" name="payment_details[card_security_code]" placeholder="Código de Seguridad de la Tarjeta" class="form-control" autocomplete="off">
	      </div>
	    </div>
	    <div class="form-group">
	      <label for="card_holder_name" class="col-sm-4 control-label">
		  Nombre del titular de la tarjeta
	      </label>
	      <div class="col-sm-8">
	        <input type="text" id="card_holder_name" name="payment_details[card_holder_name]" placeholder="Nombre del titular de la tarjeta" class="form-control" autocomplete="off">
	      </div>
	    </div>
	</div>
</div>